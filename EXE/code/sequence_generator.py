__author__ = 'njannu'
import os
import re
pwd=os.path.dirname(os.path.realpath(__file__))
with open(pwd + '/../output/iframe-classification.txt', 'r') as f:
    channel_list = f.readlines()
channel = [c.strip() for c in channel_list]
channel.insert(0,"00")
f_1 = open(pwd + '/../output/out_01_frames.txt', 'w')
out_01_re=re.compile(".*vending machine.*")
sc_re=re.compile("\(.*\)$")
for i in range(1,686):
    iframe_index = i
    classification = channel[iframe_index]
    if out_01_re.match(classification):
        score = sc_re.findall(classification)[0]
        score = score.strip(')').strip('(').split('=')[1].strip()
        if float(score) > 0.12345:
            f_1.write(str(i)+'\n')
