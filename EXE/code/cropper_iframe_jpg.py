__author__ = 'njannu'
from PIL import Image, ImageEnhance
import pytesseract
import os
import re
#(left, upper, right, lower)
box = (0, 0, 704, 290)
pwd=os.path.dirname(os.path.realpath(__file__))
for i in range(1,686):
    url = pwd + '/../output/iframe_1_jpg/iframe%06d.jpg' % i
    image = Image.open(url)
    cropped = image.crop(box)
    cropped.save(pwd + '/../output/iframe_1_jpg_cropped/iframe%06d.jpg' % i)