1.Execute extract_pic_type.sh to get all pic types
2.Execute extract_Iframe.sh to get all I frames
3.Execute extract_Pframe.sh to get all P frames
  * For all further commands, we focus only on Iframe_1 sets (from 4490F90.7z) and the same can be extended to other I frames sets as well
4.Execute cropper_iframe.py to remove the bottom green color


Now its time to start classifying the images using tensor flow


1.Install Tensor flow (https://www.tensorflow.org/versions/r0.9/get_started/os_setup.html)
2.source ~/tensorflow/bin/activate
3.location of classifier is /Users/njannu/tensorflow/lib/python2.7/site-packages/tensorflow/models/image/imagenet/classify_image.py is classifier file
4.run it to download image net data set


We can train the classifier using our own data, but this requires better cpu systems and is time taking. As a POC, we consider,
that the classification set of "vending machine" closer to one of our frame sets ( the one with a door and banner Billy Elliot)
We produce the output as out_01.
The useful links section has the list of materials using which we can train our classifier using inception3 of tensor flow


The classifier we are using will require the input in the form of jpg
1.Execute extract_Iframe_jpg.sh to get all I frames in jpg
2.Execute cropper_iframe_jpg.py to remove the bottom green color

Now its time to run the classification
1.execute python /Users/njannu/workspace/video-hack-2/EXE/code/Image_classifier.py from the activated tensor flow folder
2.The classification is restricted to only the top 1 result and is available in iframe-classification.txt
3. Run sequence_generator.py to separate the iframes based on classification results. Here we consider only the vending machine class
4. Run copy_file.py to get all i frames into a single folder
5. Now the same process is to be repeated to the other two I frame sets we have as well
6. We next need to decide the position on P frames just as explained in the H264 conversion section
7. Now finally merge the all the frames to generte final video.

Output for one class of classification from one set of I frames is pushed in the code and is also available at
https://drive.google.com/open?id=0B-LaQ2KAFPrNaVFXc0NjenBZYXM

Useful Links:
https://www.tensorflow.org/versions/r0.9/get_started/os_setup.html
https://www.tensorflow.org/versions/r0.9/tutorials/image_recognition/index.html
https://visual-recognition-demo.mybluemix.net/
https://research.googleblog.com/2016/03/train-your-own-image-classifier-with.html
https://github.com/tensorflow/models/tree/master/inception