__author__ = 'njannu'

import shutil
import os
channel='out_01_frames'
pwd=os.path.dirname(os.path.realpath(__file__))
with open(pwd + '/../output/'+channel+'.txt', 'r') as f:
    file_list = f.readlines()
files = [file.rstrip() for file in file_list]
index=1
i_dir=pwd + '/../output/cropped_Iframes_1/'
d_dir=pwd + '/../output/'+channel+'/'
for file_num in files:
    file = i_dir+"iframe%06d.png" % int(file_num)
    print 'from: ' + file + ' to : ' + d_dir+'frame%06d.png' % index
    shutil.copy(file,d_dir+'frame%06d.png' % index)
    index +=1

