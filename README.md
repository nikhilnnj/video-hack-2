# Challenge: Reverse engineering video codecs

## Objective
 Study/Analyse input files and write a code to convert them into common h264/mp4 format.

The actual challenge repository is available at http://goo.gl/HznDbo

The challenge was hosted at https://www.hackerearth.com/sprints/acuiti-hiring-hack

### Formats

  * [VOB]
  * [IVA]
  * [H264]
  * [EXE]


# IDEA #

## H264 ##
The H264 files are composed of P frames and I frames. Use ffprobe to get the details of each frame present in the video.
   
  * I‑frames are the least compressible but don't require other video frames to decode.
   
  * P‑frames can use data from previous frames to decompress and are more compressible than I‑frames.

 Next, use ffmpeg to extract all I frames and P frames from the video.
Now looking at the pattern of ffprobe, we understand that each I frame is followed by P prames that belong to the channel same as I frame. So channel change happens in I frames.

The videos have channel number shown at the top. So, if we can identify the channel number in each of the I frames, the individual channel videos can be reconstructed by appending the P frames after I frames in the order they appear.

To identify channel number from I frame, first crop the frames to focus on the channnel number. Next use Optical Character Recognition (OCR) to identify the channel number. 

Next Arrange the I and P frames in each channel as per the order they appear in ffprobe.

Finally merge all the videos within the channel.

1. Execute extract_pic_type.sh to get the pic type of all frames
    *  pict_type=I 7204 frames
    *   pict_type=P 168650 frames
    *  pict_type 175854 frames
2. Execute extract_Iframe.sh to get all I frames
    *  Now 7204 I frames available (1.19gb)
3. Execute extract_Pframe.sh to get all P frames
    * Now 168650 P frames available (26gb)
4. Execute Iframe_classifier.py.
    * This crops the i frame images to extract channel number using OCR
5. Execute sequence_generator.py
   * This follows the idea that every i frame is followed by p frames. by comparing the data from ffprobe, and the channel numbers of iframes,
The frames are arranged in theier respective channel folders
6. Execute copyfile for each channel to bring the frames into the channel folders
7. Execute merge foreach channel to get final video. There might be some noice in final videos because of p frames that are more in number and low on clarity
8. To get i frame only video, execute iframe_only_sequence_generator.py, iframe_copyfile.py, merge.sh

#### Note:
Due to huge file sizes, all files couldn't be pushed.Especially I frame and P frame files. Contact back for full explanation and the demo.

Link to output (Iframe only) : https://drive.google.com/open?id=0B-LaQ2KAFPrNY2J1VGRxRmNYaGc

Link to output (I and P frames)(Low clarity) :  https://drive.google.com/open?id=0B-LaQ2KAFPrNbmhYWmItVlAwV2c


## EXE ##

First use binwalk to extract the contents of the binary. This resulted in generation of a few certificate files and LZMA compressed files(.7z). This had almost 16 .7z files and 6 .crt files. On running ffmpeg on all the files, a few .7z files generated only audio and only 3 of them had a valid video data, namely 4490F90.7z , 3BD8EFE.7z , 2F50BC9.7z. Additionally, 2C089B.crt also had some valid video data(Wasnt able to extract this effectively).

Now, on each of the .7z files that had valid data,

1. Execute extract_pic_type.sh to get all pic types
2. Execute extract_Iframe.sh to get all I frames
3. Execute extract_Pframe.sh to get all P frames
  * For all further commands, we focus only on Iframe_1 sets (from 4490F90.7z) and the same can be extended to other I frames sets as well
4. Execute cropper_iframe.py to remove the bottom green color


We will use Tensor flow for classification


1. Install Tensor flow (https://www.tensorflow.org/versions/r0.9/get_started/os_setup.html)
2. source ~/tensorflow/bin/activate
3. location of classifier is /Users/njannu/tensorflow/lib/python2.7/site-packages/tensorflow/models/image/imagenet/classify_image.py is classifier file
4. run it to download image net data set


We can train the classifier using our own data, but this requires better cpu systems and is time taking. As a POC, we consider,
that the classification set of "vending machine" closer to one of our frame sets ( the one with a door and banner Billy Elliot)
We produce the output as out_01.
The useful links section has the list of materials using which we can train our classifier using inception3 of tensor flow


The classifier we are using will require the input in the form of jpg
 1. Execute extract_Iframe_jpg.sh to get all I frames in jpg
 2. Execute cropper_iframe_jpg.py to remove the bottom green color

Now run the classification

1. execute python /Users/njannu/workspace/video-hack-2/EXE/code/Image_classifier.py from the activated tensor flow folder
2. The classification is restricted to only the top 1 result and is available in iframe-classification.txt
3. Run sequence_generator.py to separate the iframes based on classification results. Here we consider only the vending machine class
4. Run copy_file.py to get all i frames into a single folder
5. Now the same process is to be repeated to the other two I frame sets we have as well
6. We next need to decide the position on P frames just as explained in the H264 conversion section
7. Now finally merge the all the frames to generte final video.


Output for one class of classification from one set of I frames is pushed in the code and is also available at

https://drive.google.com/open?id=0B-LaQ2KAFPrNaVFXc0NjenBZYXM


Useful Links:

* https://www.tensorflow.org/versions/r0.9/get_started/os_setup.html
* https://www.tensorflow.org/versions/r0.9/tutorials/image_recognition/index.html
* https://visual-recognition-demo.mybluemix.net/
* https://research.googleblog.com/2016/03/train-your-own-image-classifier-with.html
* https://github.com/tensorflow/models/tree/master/inception


## VOB ##
Made ffmpeg calls from a python script to convert the given VOB file into mp4 file.

## IVA ##
Used a python script to traverse through the input directory to make a list of all available files and then sort them based on the date/time parameters as obtained from the folder name. Then ran the ffmpeg command from a python script to convert all the files into a single mp4 file. (Note : The generated mp4 file is uploaded to youtube. The code also converts the input into .mkv if in case the .mp4 is damaged.(was happening in the mac). Any online tool can be used to repair this mp4 by comparing it with the other .mkv)

Youtube link : https://youtu.be/rf3bl63NzfI 3:40 stranger comes in 4:04 The car moves

Drive link : https://drive.google.com/open?id=0B-LaQ2KAFPrNWVdQUVVCc0d5aWs