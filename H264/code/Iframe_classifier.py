from PIL import Image, ImageEnhance
import pytesseract
import os
import re

#(left, upper, right, lower)
box = (30, 30, 73, 45)
pwd=os.path.dirname(os.path.realpath(__file__))
f = open(pwd + '/../output/iframe-classification.txt', 'w')
f_channel = open(pwd + '/../output/iframe-channels.txt', 'w')
ch1_re=re.compile(".*[1]$")
ch2_re=re.compile(".*[2,Z,z]$")
ch3_re=re.compile(".*[a]$")
ch4_re=re.compile(".*[4]$")
for i in range(1,7205):
    url = pwd + '/../output/Iframes/iframe%06d.png' % i
    image = Image.open(url)
    cropped = image.crop(box)
    #contrast = ImageEnhance.Contrast(cropped)
    channel_got = pytesseract.image_to_string(cropped)
    channel ="00"
    if ch1_re.match(channel_got):
        channel = "01"
    elif ch2_re.match(channel_got):
        channel = "02"
    elif ch3_re.match(channel_got):
        channel = "03"
    elif ch4_re.match(channel_got):
        channel = "04"
    else:
        print "Unhandled " + 'iframe%06d.png ___ ' % i +"channel_got: " + channel_got+ " ___ channel: " + channel+ '\n'
    save_url = pwd + '/../output/cropped/%06d -- ' % i + channel + '.png'
    cropped.save(save_url)
    f.write('iframe%06d.png ___ ' % i +"channel_got: " + channel_got+ " ___ channel: " + channel+ '\n')
    f_channel.write(channel + '\n')
f.close()
f_channel.close()

