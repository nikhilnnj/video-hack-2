__author__ = 'njannu'
import os
pwd=os.path.dirname(os.path.realpath(__file__))
with open(pwd + '/../output/iframe-channels.txt', 'r') as f:
    channel_list = f.readlines()
channel = [c.strip() for c in channel_list]
channel.insert(0,"00")
f_1 = open(pwd + '/../output/iframe_CH-01.txt', 'w')
f_2 = open(pwd + '/../output/iframe_CH-02.txt', 'w')
f_3 = open(pwd + '/../output/iframe_CH-03.txt', 'w')
f_4 = open(pwd + '/../output/iframe_CH-04.txt', 'w')

for i in range(1,7205):
    iframe_index = i
    if(channel[iframe_index]=='01'):
        f_1.write('iframe%06d.png' % iframe_index + '\n')
    elif (channel[iframe_index]=='02'):
        f_2.write('iframe%06d.png' % iframe_index + '\n')
    elif (channel[iframe_index]=='03'):
        f_3.write('iframe%06d.png' % iframe_index + '\n')
    elif (channel[iframe_index]=='04'):
        f_4.write('iframe%06d.png' % iframe_index + '\n')


f_1.close()
f_2.close()
f_3.close()
f_4.close()
f.close()
