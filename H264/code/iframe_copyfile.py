__author__ = 'njannu'

import shutil
import os
channel='iframe_CH-01'
pwd=os.path.dirname(os.path.realpath(__file__))
with open(pwd + '/../output/'+channel+'.txt', 'r') as f:
    file_list = f.readlines()
files = [file.rstrip() for file in file_list]
index=1
i_dir=pwd + '/../output/Iframes/'
d_dir=pwd + '/../output/'+channel+'/'
for file in files:
    print 'from: ' + i_dir+file + ' to : ' + d_dir+'frame%06d.png' % index
    shutil.copy(i_dir+file,d_dir+'frame%06d.png' % index)
    index += 1
